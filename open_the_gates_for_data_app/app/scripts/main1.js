



 

(function() {
    // Describe an App object with own functionalities
    var App = {
        init: function () {
            var self = this;

            this.WBCOUNTRIESAPIURL = "http://api.worldbank.org/countries/all?format=jsonP&per_page=300&prefix=jsonp_callback";
            this.DEATHRATE = "http://api.worldbank.org/countries/{0}/indicators/SP.DYN.CDRT.IN?format=jsonP&per_page=14000&prefix=jsonp_callback";
            this.DEATHCOMMU = "http://api.worldbank.org/countries/{0}/indicators/SH.DTH.COMM.ZS?format=jsonP&per_page=300&prefix=jsonp_callback";
            this.DEATHNONCOMMU = "http://api.worldbank.org/countries/{0}/indicators/SH.DTH.NCOM.ZS?format=jsonP&per_page=300&prefix=jsonp_callback";
            this.CO2 = "http://api.worldbank.org/countries/{0}/indicators/EN.ATM.CO2E.PC?format=jsonP&per_page=300&prefix=jsonp_callback";
            this.INJURY = "http://api.worldbank.org/countries/{0}/indicators/SH.DTH.INJR.ZS?format=jsonP&per_page=300&prefix=jsonp_callback";
            this.IMMUMEASELS = "http://api.worldbank.org/countries/{0}/indicators/SH.IMM.MEAS?format=jsonP&per_page=300&prefix=jsonp_callback";
            this.IMMUDPT = "http://api.worldbank.org/countries/{0}/indicators/SH.IMM.IDPT?format=jsonP&per_page=300&prefix=jsonp_callback";
            
            this._dataCountries = null;// Variable for the list of countries
            this._dataCountry = {
                "info": null,// Information of the country comes from the list of countries
                "deathrate": null,// Variable for the list of financial sector per year
                "deathcommu": null,
                "deathnoncommu": null,
                "injury": null,
                "immumeasels": null,
                "immudpt": null,
                
            };// Variable for the details of a country
            // Create the UsersDbContext object via clone
            this._usersDbContext = UsersDbContext;
            // Initialize the UsersDbContext given a certain namespace aka connection string
            this._usersDbContext.init('dds.users');

            this._createSignUpForm = document.querySelector('#signup-create-form');
            this._createLoginForm = document.querySelector('#login-create-form');
            this._createForgotPasswordForm = document.querySelector('#forgot_password-create-form');

            // Handlebars Cache
            this._hbsCache = {};// Handlebars cache for templates
            this._hbsPartialsCache = {};// Handlebars cache for partials

            // Create a clone from the JayWalker object
            this._jayWalker = JayWalker;
            this._jayWalker.init();
            this._jayWalker._countryDetailsJSONPLoaded.add(function (iso2code) {
                self.loadDatasetsFromCountry(iso2code);// Test: load details data from country
            });

            //Register Listeners

            console.dir(this._usersDbContext);

          
        loadCountriesFromWorldBankAPI: function () {
            // Closure
            var self = this, url = String.format(this.WBCOUNTRIESAPIURL, new Date().getTime());

            // Load JSONP from corresponding API with certain URL
            // JSONP Callback is defined by a function name in this case
            // prefix=jsonp_callback. The Utils object contains a new function
            // which can handle the callback
            Utils.getJSONPByPromise(url).then(
                function (data) {
                    if (data != null) {
                        var countries = data[1]; // Get the countries from JSON (second item from array, first item is paging)
                        //console.log("countries: " + countries);
                        var countriesFiltered = _.filter(countries, function (country) {
                            return !(/\d/.test(country.iso2Code));
                        });// First remove weird countries with LoDash + Assign data as value flor global variable _dataCountries within the App
                        var badISO2Codes = ['XT', 'XN', 'ZG', 'ZF', 'OE', 'XS', 'XR', 'XU', 'XQ', 'XP', 'ZQ', 'XO', 'XN', 'XM', 'XL', 'ZJ', 'XJ', 'XY', 'XE', 'EU', 'XC', 'JG', 'XD'];

                        self._dataCountries = _.filter(countriesFiltered, function (country) {
                            var validCountry = true, i = 0;

                            while (validCountry && i < badISO2Codes.length) {
                                if (country.iso2Code == badISO2Codes[i]) {
                                    validCountry = false;
                                } else {
                                    i++;
                                }
                            }

                            return validCountry;
                        });// Filter (weird codes: XT, XN, ZG, ZF, OE, XS, XR, XU, XQ, XP, ZQ, XO, XN, XM, XL, ZJ, XJ, XY, XE, EU, XC, JG)
                        //console.log(self._dataCountries);
                        self._dataCountries = _.sortBy(self._dataCountries, function (country) {
                            return country.name;
                        });// Sorting on country name
                        self.updateCountriesUI('countries-tiles', '#countries-tiles-template');// Call updateCountriesUI method when successful*/


                    }
                },
                function (status) {
                    console.log(status);
                }
            );

        },
        updateCountriesUI: function (hbsTmplName, hbsTmplId) {
            if (!this._hbsCache[hbsTmplName]) {
                var src = document.querySelector(hbsTmplId).innerHTML;// Get the contents from the specified hbs template
                this._hbsCache[hbsTmplName] = Handlebars.compile(src);// Compile the source and add it to the hbs cache
            }
            document.querySelector('.countries-list').innerHTML = this._hbsCache[hbsTmplName](this._dataCountries);// Write compiled content to the appropriate container
        },
        loadDatasetsFromCountry: function (iso2code) {
            var selectedCountry = _.find(this._dataCountries, function (country) {
                return country.iso2Code == iso2code;
            });
            if (selectedCountry != null && typeof selectedCountry != undefined) {
                this._dataCountry.info = selectedCountry;
            }


            this.loadDeathrateFromCountryFromWorldBankAPI(iso2code);
            this.loadNonCommFromCountryFromWorldBankAPI(iso2code);
            this.loadCommCountryFromWorldBankAPI(iso2code);
            this.loadInjuryFromWorldBankAPI(iso2code);
            this.loadCO2CountryFromWorldBankAPI(iso2code); 
            this.loadMeazelsCountryFromWorldBankAPI(iso2code);
            this.loadDTPFromCountryFromWorldBankAPI(iso2code);
            

        },

        
        loadDeathrateFromCountryFromWorldBankAPI: function (iso2code) {
            // Closure
            var self = this, url = String.format(this.DEATHRATE, iso2code);


            // Load JSONP from corresponding API with certain URL
            // JSONP Callback is defined by a function name in this case
            // prefix=jsonp_callback. The Utils object contains a new function
            // which can handle the callback
            Utils.getJSONPByPromise(url).then(
                function (data) {
                    if (data != null) {
                        //console.log("data received");

                        var deathrate = data[1]; // Get the deathrate from the selected country from JSON (second item from array, first item is paging)
                        
                        var deathrateFiltered = _.filter(deathrate, function (DeathratePerYear) {
                            return DeathratePerYear.value != null;
                        });// First remove all years where value is null with LoDash
                        deathrateFiltered = _.sortBy(deathrateFiltered, function (DeathratePerYear) {
                            return DeathratePerYear.year;
                        });// Sorting on year

                        self._dataCountry.deathrate = deathrateFiltered; 

                        self.updateCountryDetailsUI('country-details', '#country-details-template');// Call updateCountryDetailsUI method when successful


                        //for (var i =0; i < financialsector.length;i++)
                        //{
                        //console.log('test' + financialsector[i].value);
                        //}
                    } else {
                        console.log("no data");
                    }
                }
            );
        },
        //LOAD ECONOMIC MANAGEMENT
        loadEconomicManagementFromCountryFromWorldBankAPI: function (iso2code) {
            var self = this, url = String.format(this.ECONOMICMANAGEMENTAPI, iso2code);

            Utils.getJSONPByPromise(url).then(
                function (data) {
                    if (data != null) {
                        var economicmanagement = data[1];
                        //console.log("economice management: " + economicmanagement);
                        var economicmanagementFiltered = _.filter(economicmanagement, function (economicmanagementPerYear) {
                            return economicmanagementPerYear.value != null;
                        });
                        economicmanagementFiltered = _.sortBy(economicmanagementFiltered, function (economicmanagementPerYear) {
                            return economicmanagementPerYear.year;
                        });
                        self._dataCountry.economicmanagement = economicmanagementFiltered;
                        self.updateCountryDetailsUI('country-details', '#country-details-template');
                    }
                }
            );
        },
        //LOAD PROPERTY RIGHTS AND RULES
        loadPropertyRightsRuleFromCountryFromWorldBankAPI: function (iso2code) {
            var self = this, url = String.format(this.PROPERTYRIGHTSRULEAPI, iso2code);

            Utils.getJSONPByPromise(url).then(
                function (data) {
                    if (data != null) {
                        var propertyrightsrule = data[1];
                        //console.log("property rights and rules: " + propertyrightsrule);
                        var propertyrightsruleFiltered = _.filter(propertyrightsrule, function (propertyrightsrulePerYear) {
                            return propertyrightsrulePerYear.value != null;
                        });
                        propertyrightsruleFiltered = _.sortBy(propertyrightsruleFiltered, function (propertyrightsrulePerYear) {
                            return propertyrightsrulePerYear.year;
                        });
                        self._dataCountry.propertyrightsrule = propertyrightsruleFiltered;
                        self.updateCountryDetailsUI('country-details', '#country-details-template');
                    }
                }
            );
        },
        //LOAD ENERGY
        loadEnergyFromCountryFromWorldBankAPI: function (iso2code) {
            var self = this, url = String.format(this.ENERGYAPI, iso2code);

            Utils.getJSONPByPromise(url).then(
                function (data) {
                    if (data != null) {
                        var energy = data[1];
                        //console.log("energy: " + energy);
                        var energyFiltered = _.filter(energy, function (energyPerYear) {
                            return energyPerYear.value != null;
                        });
                        energyFiltered = _.sortBy(energyFiltered, function (energyPerYear) {
                            return energyPerYear.year;
                        });// Sorting on year
                        self._dataCountry.energy = energyFiltered;
                        self.updateCountryDetailsUI('country-details', '#country-details-template');
                    }
                }
            );
        },
        //LOAD AGRICULTURAL
        loadAgricultarFromCountryFromWorldBankAPI: function (iso2code) {
            var self = this, url = String.format(this.AGRICULTURALAPI, iso2code);

            Utils.getJSONPByPromise(url).then(
                function (data) {
                    if (data != null) {
                        var agricultural = data[1];
                        //console.log("agricultural: " + agricultural);
                        var agriculturalFiltered = _.filter(agricultural, function (agriculturalPerYear) {
                            return agriculturalPerYear.value != null;
                        });
                        agriculturalFiltered = _.sortBy(agriculturalFiltered, function (agriculturalPerYear) {
                            return agriculturalPerYear.year;
                        });// Sorting on year
                        self._dataCountry.agricultural = agriculturalFiltered;
                        self.updateCountryDetailsUI('country-details', '#country-details-template');
                    }
                }
            );
        },
        //LOAD FEMALEEMPLOYMENTAGRI
        loadFemaleEmploymentAgriFromCountryFromWorldBankAPI: function (iso2code) {
            var self = this, url = String.format(this.FEMALEEMPLOYMENTAGRI, iso2code);

            Utils.getJSONPByPromise(url).then(
                function (data) {
                    if (data != null) {
                        var femaleempagri = data[1];
                        //console.log("agricultural: " + agricultural);
                        var femaleempagriFiltered = _.filter(femaleempagri, function (femaleempagriPerYear) {
                            return femaleempagriPerYear.value != null;
                        });
                        femaleempagriFiltered = _.sortBy(femaleempagriFiltered, function (femaleempagriPerYear) {
                            return femaleempagriPerYear.year;
                        });// Sorting on year
                        self._dataCountry.femaleempagri = femaleempagriFiltered;
                        self.updateCountryDetailsUI('country-details', '#country-details-template');

                    }
                }
            );
        },
        //LOAD MALEEMPLOYMENTAGRI
        loadMaleEmploymentAgriFromCountryFromWorldBankAPI: function (iso2code) {
            var self = this, url = String.format(this.MALEEMPLOYMENTAGRI, iso2code);

            Utils.getJSONPByPromise(url).then(
                function (data) {
                    if (data != null) {
                        var maleempagri = data[1];
                        //console.log("agricultural: " + agricultural);
                        var maleempagriFiltered = _.filter(maleempagri, function (maleempagriPerYear) {
                            return maleempagriPerYear.value != null;
                        });
                        maleempagriFiltered = _.sortBy(maleempagriFiltered, function (maleempagriPerYear) {
                            return maleempagriPerYear.year;
                        });// Sorting on year
                        self._dataCountry.maleempagri = maleempagriFiltered;
                        self.updateCountryDetailsUI('country-details', '#country-details-template');

                    }
                }
            );
        },
        //LOAD FEMALEEMPLOYMENTINDUSTRY
        loadFemaleEmploymentIndustryFromCountryFromWorldBankAPI: function (iso2code) {
            var self = this, url = String.format(this.FEMALEEMPLOYMENTINDUSTRY, iso2code);

            Utils.getJSONPByPromise(url).then(
                function (data) {
                    if (data != null) {
                        var femaleempindustry = data[1];
                        //console.log("agricultural: " + agricultural);
                        var femaleempindustryFiltered = _.filter(femaleempindustry, function (femaleempindustryPerYear) {
                            return femaleempindustryPerYear.value != null;
                        });
                        femaleempindustryFiltered = _.sortBy(femaleempindustryFiltered, function (femaleempindustryPerYear) {
                            return femaleempindustryPerYear.year;
                        });// Sorting on year
                        self._dataCountry.femaleempindustry = femaleempindustryFiltered;
                        self.updateCountryDetailsUI('country-details', '#country-details-template');

                    }
                }
            );
        },
        //LOAD MALEEMPLOYMENTINDUSTRY
        loadMaleEmploymentIndustryFromCountryFromWorldBankAPI: function (iso2code) {
            var self = this, url = String.format(this.MALEEMPLOYMENTINDUSTRY, iso2code);

            Utils.getJSONPByPromise(url).then(
                function (data) {
                    if (data != null) {
                        var maleempindustry = data[1];
                        //console.log("agricultural: " + agricultural);
                        var maleempindustryFiltered = _.filter(maleempindustry, function (maleempindustryPerYear) {
                            return maleempindustryPerYear.value != null;
                        });
                        maleempindustryFiltered = _.sortBy(maleempindustryFiltered, function (maleempindustryPerYear) {
                            return maleempindustryPerYear.year;
                        });// Sorting on year
                        self._dataCountry.maleempindustry = maleempindustryFiltered;
                        self.updateCountryDetailsUI('country-details', '#country-details-template');

                    }
                }
            );
        },
        //LOAD FEMALEEMPLOYMENTSERV
        loadFemaleEmploymentServFromCountryFromWorldBankAPI: function (iso2code) {
            var self = this, url = String.format(this.FEMALEEMPLOYMENTSERV, iso2code);

            Utils.getJSONPByPromise(url).then(
                function (data) {
                    if (data != null) {
                        var femaleempserv = data[1];
                        //console.log("agricultural: " + agricultural);
                        var femaleempservFiltered = _.filter(femaleempserv, function (femaleempservPerYear) {
                            return femaleempservPerYear.value != null;
                        });
                        femaleempservFiltered = _.sortBy(femaleempservFiltered, function (femaleempservPerYear) {
                            return femaleempservPerYear.year;
                        });// Sorting on year
                        self._dataCountry.femaleempserv = femaleempservFiltered;
                        self.updateCountryDetailsUI('country-details', '#country-details-template');

                    }
                }
            );
        },
        //LOAD MALEEMPLOYMENTSERV
        loadMaleEmploymentServFromCountryFromWorldBankAPI: function (iso2code) {
            var self = this, url = String.format(this.MALEEMPLOYMENTSERV, iso2code);

            Utils.getJSONPByPromise(url).then(
                function (data) {
                    if (data != null) {
                        var maleempserv = data[1];
                        //console.log("agricultural: " + agricultural);
                        var maleempservFiltered = _.filter(maleempserv, function (maleempservPerYear) {
                            return maleempservPerYear.value != null;
                        });
                        maleempservFiltered = _.sortBy(maleempservFiltered, function (maleempservPerYear) {
                            return maleempservPerYear.year;
                        });// Sorting on year
                        self._dataCountry.maleempserv = maleempservFiltered;
                        self.updateCountryDetailsUI('country-details', '#country-details-template');
                    }
                }
            );
        },
        //LOAD ELECTRICITY
        loadElectricityFromCountryFromWorldBankAPI: function (iso2code) {
            var self = this, url = String.format(this.ELECTRICITYAPI, iso2code);

            Utils.getJSONPByPromise(url).then(
                function (data) {
                    if (data != null) {
                        var electricity = data[1];
                        //console.log("electricity: " + electricity);
                        var electricityFiltered = _.filter(electricity, function (electricityPerYear) {
                            return electricityPerYear.value != null;
                        });
                        electricityFiltered = _.sortBy(electricityFiltered, function (electricityPerYear) {
                            return electricityPerYear.year;
                        });// Sorting on year
                        self._dataCountry.electricity = electricityFiltered;
                        self.updateCountryDetailsUI('country-details', '#country-details-template');
                    }
                }
            );
        },
        //LOAD PRIMARY AND SECUNDARY SCHOOL
        loadPrimarySecundaryFromCountryFromWorldBankAPI: function (iso2code) {
            var self = this, url = String.format(this.PRIMARYSECUNDARYAPI, iso2code);

            Utils.getJSONPByPromise(url).then(
                function (data) {
                    if (data != null) {
                        var primarysecundadry = data[1];
                        //console.log("primarysecundadry: " + primarysecundadry);
                        var primarysecundadryFiltered = _.filter(primarysecundadry, function (primarysecundadryPerYear) {
                            return primarysecundadryPerYear.value != null;
                        });
                        primarysecundadryFiltered = _.sortBy(primarysecundadryFiltered, function (primarysecundadryPerYear) {
                            return primarysecundadryPerYear.year;
                        });// Sorting on year
                        self._dataCountry.primarysecundadry = primarysecundadryFiltered;
                        self.updateCountryDetailsUI('country-details', '#country-details-template');
                    }
                }
            );
        },
        //LOAD TERTAIRY SCHOOL
        loadTertairyFromCountryFromWorldBankAPI: function (iso2code) {
            var self = this, url = String.format(this.TERTAIRYAPI, iso2code);

            Utils.getJSONPByPromise(url).then(
                function (data) {
                    if (data != null) {
                        var tertairy = data[1];
                        //console.log("tertairy: " + tertairy);
                        var tertairyFiltered = _.filter(tertairy, function (tertairyPerYear) {
                            return tertairyPerYear.value != null;
                        });
                        tertairyFiltered = _.sortBy(tertairyFiltered, function (tertairyPerYear) {
                            return tertairyPerYear.year;
                        });// Sorting on year
                        self._dataCountry.tertairy = tertairyFiltered;
                        self.updateCountryDetailsUI('country-details', '#country-details-template');
                    }
                }
            );
        },

        updateCountryDetailsUI: function (hbsTmplName, hbsTmplId) {
            if (!this._hbsCache[hbsTmplName]) {
                var src = document.querySelector(hbsTmplId).innerHTML;// Get the contents from the specified hbs template
                this._hbsCache[hbsTmplName] = Handlebars.compile(src);// Compile the source and add it to the hbs cache
            }
            document.querySelector('.country-details').innerHTML = this._hbsCache[hbsTmplName](this._dataCountry);// Write compiled content to the appropriate container
            //console.log(this._dataCountry);
            this.createFinancialSectorAreaGraphForCountry();
            this.createFemaleEmploymentSectorGraphForCountry();

        },
        createFinancialSectorAreaGraphForCountry: function () {
            if (this._dataCountry.financialsector != null) {
                var labels = [], series = [];
                _.each(this._dataCountry.financialsector, function (item) {
                    labels.push(item.date);
                    series.push(parseFloat(item.value));
                    //console.log("item: " + item)
                });

                var options = {
                    low: _.min(_.pluck(this._dataCountry.financialsector, 'value')),
                    hight: _.max(_.pluck(this._dataCountry.financialsector, 'value'))
                };

                var data = {
                    labels: labels,
                    series: [series]
                };
                // Create a new line chart object where as first parameter we pass in a selector that is resolving to our chart container element. The Second parameter is the actual data object.
                //new Chartist.Line('.ct-chart-detail', data, options);
            }

        },
        createFemaleEmploymentSectorGraphForCountry: function () {
            if (this._dataCountry.femaleempindustry != null) {


                for (var i = 0; i < this._dataCountry.femaleempindustry.length; i++) {
                    if (this._dataCountry.femaleempindustry[i].date == "2012") {
                        console.log(this._dataCountry.femaleempindustry[i].value);
                        var data = {
                            series: [this._dataCountry.femaleempindustry[i].value, this._dataCountry.femaleempagri[i].value, this._dataCountry.femaleempserv[i].value],
                            labels: ["industry", "agriculture", "services"]

                        };
                    }

                }
                var responsiveOptions = [
                    ['screen and (min-width: 640px)', {
                        chartPadding: 30,
                        labelOffset: 100,
                        labelDirection: 'explode',
                        labelInterpolationFnc: function (value) {
                            return value;
                        }
                    }],
                    ['screen and (min-width: 1024px)', {
                        labelOffset: 80,
                        chartPadding: 20
                    }]
                ];
            }

            new Chartist.Pie('.ct-chart-detail2',data, {
             donut: true,
             donutWidth: 60,
             startAngle: 270,
             total: 200,
             showLabel: true
             },responsiveOptions);
        },
        categoryIndustry: function () {

            var self = this;
            if (this._dataCountries != null) {
                alert('test');
                console.log(self._dataCountries);
                for (var i = 0; i < countries.length; i++) {
                    for (var i = 0; i < self._dataCountries.maleempindustry.length; i++) {
                        if (self._dataCountry.maleempindustry[i].date == "2012") {
                            if (self._dataCountry.maleempindustry[i].value > 30) {
                                console.log(self._dataCountry.maleempindustry[i].value);

                            }
                        }

                    }

                }
            }
        },
        toggleinfo: function () {
            var infoToggle = querySelectorAll(".infotoggle");
            infoToggle.addEventListener('click', function () {
               var tekst =  querySelectorAll("span.text-content");
                        tekst.style.display = 'block';
                    });
                }

    };



    App.init();
    // Intialize the application

   document.getElementById("logout").addEventListener("click", loggedout);
    document.getElementById("logout-mobile").addEventListener("click", loggedout);


    function loggedout(){
        document.getElementById("header-wrapper").style.display = "none";
        var elems = document.getElementsByClassName("page");
        for(var i = 0; i < elems.length; i++) {
            elems[i].style.top = '0px';
        }

        var activeUser = App._usersDbContext.getLoggedInUser();

        if (activeUser == null || (activeUser != null && activeUser.length == 0)) {} else {
            App._usersDbContext.deleteActiveUser(activeUser);
        }
    };

    document.getElementById("favorite").addEventListener("click", addToFavorites);

    function addToFavorites(){
        var iso2Code = document.getElementById("country-title").getAttribute("data-id");
        document.getElementById("favorite").className = "fa fa-star";

        App._usersDbContext.addFavorite(iso2Code);
    };
    var WebPage = require('webpage');
    page = WebPage.create();
    page.open('http://google.com');
    page.onLoadFinished = function() {
        page.render('googleScreenShot' + '.png');
        phantom.exit();}
   /* document.getElementById("test").addEventListener("click"){
        document.getElementById("inputProfile").removeAttr("enabled");}

*/

    /*


     registerNavigationToggleListeners: function () {
     var toggles = document.querySelectorAll('.navigation-toggle');

     if (toggles != null && toggles.length > 0) {
     var toggle = null;

     for (var i = 0; i < toggles.length; i++) {
     toggle = toggles[i];
     toggle.addEventListener('click', function (ev) {
     ev.preventDefault();

     document.querySelector('body').classList.toggle(this.dataset.navtype);

     return false;
     });
     }
     }
     },
     */



})();